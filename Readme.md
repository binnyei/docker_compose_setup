# Docker-compose setup with MariaDB, Flask and Php
# Goals
This is my playground for getting know docker(-compose), flask, php and other previous used web technologies.
This setup enables to run a database and connect with a flask and php website to it.
# Documentation
Documentation is in the so named directory, where I use LaTex for it. Therefore this Readme is rather short.