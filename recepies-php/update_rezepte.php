<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{
    require_once "db access/db_connection.php"; // Einbinden von Datenbankverbindungsaufbau


if ($stmt = $mysqli->prepare("UPDATE rezepte SET name=?, anleitung=?, zutaten=? WHERE id=?")){
            $id=$_POST["rezept_id_post"];
            $name=$_POST["rezept_name"];
            $anleitung=$_POST["rezept_anleitung"];
            $zutaten=$_POST["rezept_zutaten"];

            $stmt->bind_param("ssss", $name, $anleitung, $zutaten, $id);
            $stmt->execute();
            $stmt->close();
            $mysqli->close();
                }



    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "edit_rezepte.php";
    header("Location: http://$host$uri/$extra");
    

} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");                   
        }
?>