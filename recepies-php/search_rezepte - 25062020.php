<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{
    require_once "db access/db_connection.php"; // Einbinden von Datenbankverbindungsaufbau
    
?>                                                                  <!--Von hier bis nach dem else brauch ich das Script zum Session erstellen-->


<html>
<!-- Kommentare in HTML -->
    
<head>    
     <title>Christian's Homepage</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch.  -->
    <link rel="stylesheet" type="text/css" href="style.css"> 
    <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="mobile.css"> 
    <script src="jquery-3.5.1.js"></script>
	<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
    <script src="jquery-ui/jquery-ui.js"></script>
  
    <script src="zInput.js"></script>

</head>


    
<body>
	<div class="menu_top">
    <ul>
		<li><a href="input_rezepte.php" target="_self">Neu</a></li>
        <li><a href="search_rezepte.php" target="_self">Suchen</a></li> 
        <li><a href="sort_rezepte.php" target="_self">Sortieren</a></li> 
        <li><a href="edit_rezepte.php" target="_self">Editieren</a></li> 
        <div id="topmenu_right">
        <li><a href="logout.php" target="_self">logout</a></li>
        </div>
    </ul>
    </div>
    
    <div class="abstand">
        
    </div>
	<div class="content">

        

<!-- Hier wird im selben Script bearbeitet:-->
<!-- <form action="Formulare.php" method="get">  Selben Namen reinschreiben wie das Script in dem wir gerade sind -->
<!-- Den Vornamen lasse ich hier gespeichert, falls keiner gesetzt ist wird der String überschrieben.-->


<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES);?>" method="post">  <!-- Namen von dem jetzigen Script selbst beziehen, damit man es nicht händisch ändern muss wenn man die Struktur ändert-->
    <input type="text" name="rezept_name" size="50" maxlength="100" id="rezept_name" value="" class="input_fields" placeholder="Rezeptnamen eingeben oder suchen klicken für alle Rezepte."/>
    
	<div id="auswahl_suche">
        <input type="radio" name="suche_auswahl" value="Name" id="suche_name" title="Name"checked>
        <input type="radio" name="suche_auswahl" id="suche_zutaten" value="Zutaten" title="Zutaten">
    </div>
    
    <input type="submit" value="Suchen" class="button" id="suchen"/>
    
    <script>
        $("#auswahl_suche").zInput();

    </script>
   
</form>
        

        
                <?php
                if(isset($_POST['rezept_name'])){
                $suche=$_POST['rezept_name']; 
                $suche= "%".$suche."%";
                //Suche nach Namen oder Zutaten. Ich kann die Suche je nach Auswahl von Radiobutton "suche_Auswahl" vornehmen
                if($_POST['suche_auswahl']=='Name'){
                             if ($stmt = $mysqli->prepare("SELECT id, name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE name LIKE ? ORDER BY hauptkategorie, nebenkategorie")) 
                            {
                                $stmt->bind_param("s", $suche);
                                $stmt->execute();

                                //printf("Error: %s.\n", $stmt->error); //Display error messages if execute does not work!!!

                                $stmt->bind_result($id, $name, $anleitung, $zutaten, $type, $hauptkategorie, $nebenkategorie);
                                
                                
                                echo "<table>\n";
                                echo "<tr>\n";
                                echo "<th>Id</th>";    
                                echo "<th>Name</th>";
                                echo "<th>Zutaten</th>";
                                echo "<th>Anleitung</th>";
                                echo "<th>Hauptkategorie</th>";
                                echo "<th>Unterkategorie</th>";
                                echo "<th>Ernährungstyp</th>";
                                echo "</tr>";
                                while($stmt->fetch()){
                                    echo "<tr>\n\t<td>"
                                        .htmlspecialchars($id)
                                        ."</td>\n\t<td>"
                                        .htmlspecialchars($name)
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($zutaten))        // Gibt den Zeilenumbruch an aber macht auch <> unschädlich!
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($anleitung))
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($hauptkategorie))
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($nebenkategorie))
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($type))
                                        ."</td></tr>";
                                }
                                    


                                $stmt->close();
                                $mysqli->close();

                            }

                            else{echo ("Hat nicht funktioniert");}
                          
                    }
                        else{
                               if ($stmt = $mysqli->prepare("SELECT id, name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE zutaten LIKE ? ORDER BY hauptkategorie, nebenkategorie")) 
                                {
                                    $stmt->bind_param("s", $suche);
                                    $stmt->execute();

                                    //printf("Error: %s.\n", $stmt->error); //Display error messages if execute does not work!!!

                                    $stmt->bind_result($id, $name, $anleitung, $zutaten, $type, $hauptkategorie, $nebenkategorie);
                                    
                                    echo "<table>\n";
                                    echo "<tr>\n";
                                    echo "<th>Id</th>";    
                                    echo "<th>Name</th>";
                                    echo "<th>Zutaten</th>";
                                    echo "<th>Anleitung</th>";
                                    echo "<th>Hauptkategorie</th>";
                                    echo "<th>Unterkategorie</th>";
                                    echo "<th>Ernährungstyp</th>";
                                    echo "</tr>";
                                    while($stmt->fetch()){
                                        echo "<tr>\n\t<td>"
                                            .htmlspecialchars($id)
                                            ."</td>\n\t<td>"
                                            .htmlspecialchars($name)
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($zutaten))        // Gibt den Zeilenumbruch an aber macht auch <> unschädlich!
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($anleitung))
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($hauptkategorie))
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($nebenkategorie))
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($type))
                                            ."</td></tr>";
                                    }


                                    $stmt->close();
                                    $mysqli->close();

                                }
                               

                                else{echo ("Hat nicht funktioniert");}
                               
                    
                }

                                  
                    
                    

                    
                    
                    }
            ?> 
                        
                        
                       
        


    </div>
    
    <script>
        // Check if DIV abstand is available. It is not when the mobile CSS is taken.
        var div_heigth = $(".abstand").height();
        if(div_heigth==0){
            alert("mobile Seite");
            var is_mobile =true;
        }
        else{
            var is_mobile=false;
        }
        alert (is_mobile);
    </script>
    
    
</body>
</html>


<?php
} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");                   
        }