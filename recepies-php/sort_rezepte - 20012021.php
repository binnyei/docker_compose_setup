<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{
    require_once "db access/db_connection.php"; // Einbinden von Datenbankverbindungsaufbau
    
?>                                                                  <!--Von hier bis nach dem else brauch ich das Script zum Session erstellen-->


<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Christian's Homepage</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch.  -->
    <link rel="stylesheet" type="text/css" href="style.css"> 
    <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="mobile.css"> 
    <script src="jquery-3.5.1.js"></script>
	<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
    <script src="jquery-ui/jquery-ui.js"></script>
</head>


    
<body>
	<div class="menu_top">
    <ul>
		<li><a href="input_rezepte.php" target="_self">Neu</a></li>
        <li><a href="search_rezepte.php" target="_self">Suchen</a></li> 
        <li><a href="sort_rezepte.php" target="_self">Sortieren</a></li> 
        <li><a href="edit_rezepte.php" target="_self">Editieren</a></li> 
        <div id="topmenu_right">
            <li><a href="logout.php" target="_self">logout</a></li>
        </div>
    </ul>
    </div>
    
    <div class="abstand">
        
    </div>
	<div class="content">

        
<!-- Hier wird im selben Script bearbeitet:-->
<!-- <form action="Formulare.php" method="get">  Selben Namen reinschreiben wie das Script in dem wir gerade sind -->
<!-- Den Vornamen lasse ich hier gespeichert, falls keiner gesetzt ist wird der String überschrieben.-->


<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES);?>" method="post">  <!-- Namen von dem jetzigen Script selbst beziehen, damit man es nicht händisch ändern muss wenn man die Struktur ändert-->
	
    <select name="hauptkategorie" id="hauptkategorie"> 
        <option value="" id="first_placeholder">Hauptkategorie</option>
    <?php
        //Da kommt der ganze Scheiß mit einer Dropbox befüllen.
        //Ich rufe zuerst meine Tabelle mit den Einträgen auf und fülle dann alles in ein Array ein über eine Schleife.
        //Das alles mache ich innerhalb eines Selects mit mehrfachen Php scripten
        $ergebnis = $mysqli->query("SELECT hauptkategorie FROM hauptkategorie");
        $i=0;
        while($zeile = $ergebnis->fetch_assoc())    // Hier hole ich alle Sätze von einer Tabelle und schreibe sie in ein array
        {
        $menu[$i] = $zeile['hauptkategorie'];
    ?>    
    
        <option value="<?php echo $menu[$i] ?>"><?php echo $menu[$i] ?></option>
    <?php 
        $i++;    
        }
    ?>
  
    </select>
    oder
   
    <select name="unterkategorie" id="unterkategorie">
        <option value="" id="first_placeholder">Unterkategorie</option>
    <?php
        $ergebnis = $mysqli->query("SELECT unterkategorie FROM nebenkategorie"); // Nebenkategorie -> Tabelle, unterkategorie -> Columnname
        $i=0;
        while($zeile = $ergebnis->fetch_assoc())    // Hier hole ich alle Sätze von einer Tabelle und schreibe sie in ein array
        {
        $menu[$i] = $zeile['unterkategorie'];   // Unterkategorie ist der Zeilenname
    ?>    
    
        <option value="<?php echo $menu[$i] ?>"><?php echo $menu[$i] ?></option>
    <?php 
        $i++;    
        }
    ?>
  
    </select>

    und

    <select name="typ" id="typ">
        <option value="" id="first_placeholder">Ernährungstyp</option>
    <?php
        $ergebnis = $mysqli->query("SELECT type FROM typ"); 
        $i=0;
        while($zeile = $ergebnis->fetch_assoc())    
        {
        $menu[$i] = $zeile['type'];   
    ?>    
    
        <option value="<?php echo $menu[$i] ?>"><?php echo $menu[$i] ?></option>
    <?php 
        $i++;    
        }
    ?>
  
    </select>
	
	<input type="submit" value="Suchen" class="button"/>
    
    
    
    <input type="button" value="Drucken" class="button" id="print"/>
<script>
    
    //Funktioniert noch nicht. Möchte den Button nur anzeigen wenn eine Tabelle schon vorhanden ist.
    //Möchte ein Inhaltsverzeichnis drucken
    //Möchte es in einem gewissen Stil drucken
    //Drucken nach Auswahl von Haupt/Unterkategorie -> Sortieren nach ID und nicht nach Alphabet fürs Drucken!!!
    
    $("#print").click(function(){
       var divToPrint = $("#printtable");
           
    newWin= window.open("");
       newWin.document.write(divToPrint.outerHTML);
       newWin.print();
       newWin.close();
    });
</script>    
    
    
    
</form>

<?php    // das ist die SUCHE nach dem NAMEN
    $suche=$_POST['hauptkategorie'];
    $suche1=$_POST['unterkategorie'];
    $suche2=$_POST['typ'];
    
    if(isset($_POST['hauptkategorie'])||isset($_POST['unterkategorie'])||isset($_POST['typ']))
    {
    //if ($stmt = $mysqli->prepare("SELECT name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE (hauptkategorie LIKE ? OR nebenkategorie LIKE ?) && type LIKE ? ORDER BY hauptkategorie, nebenkategorie")) 
    
    if(isset($suche)&&$suche!=NULL&&isset($suche1)&&$suche1!=NULL){
    echo "Beide Kategorien ausgewäht";
    //if haupt and nebenkategorie are selected -> other mysqli Prepared statement than when only one kategorie is selected    
    }

    if ($stmt = $mysqli->prepare("SELECT name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE (hauptkategorie LIKE ? OR nebenkategorie LIKE ?) && type LIKE ? ORDER BY hauptkategorie, nebenkategorie"))     
    {
        $stmt->bind_param("sss", $suche, $suche1, $suche2);
        $stmt->execute();
        //printf("Error: %s.\n", $stmt->error); //Display error messages if execute does not work!!!
        $stmt->bind_result($name, $anleitung, $zutaten, $type, $hauptkategorie, $nebenkategorie);
        
        echo "<table id='printtable'>\n";
        echo "<tr>\n";
        echo "<th>Name</th>";
        echo "<th>Zutaten</th>";
        echo "<th>Anleitung</th>";
        echo "<th>Hauptkategorie</th>";
        echo "<th>Unterkategorie</th>";
        echo "<th>Ernährungstyp</th>";
        echo "</tr>";
        while($stmt->fetch()){
            echo "<tr>\n\t<td>"
                .htmlspecialchars($name)
                ."</td>\n\t<td>"
                .nl2br(htmlspecialchars($zutaten))        // Gibt den Zeilenumbruch an aber macht auch <> unschädlich!
                ."</td>\n\t<td>"
                .nl2br(htmlspecialchars($anleitung))
                ."</td>\n\t<td>"
                .nl2br(htmlspecialchars($hauptkategorie))
                ."</td>\n\t<td>"
                .nl2br(htmlspecialchars($nebenkategorie))
                ."</td>\n\t<td>"
                .nl2br(htmlspecialchars($type))
                ."</td></tr>";
        }
        

        $stmt->close();
        $mysqli->close();
       
    }
    
    else{echo "Hat nicht funktioniert";}
    // das ist die SUCHE nach KATEGORIE
    }
?> 

    
</div>    
</body>
</html>


<?php
} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");                   
        }