<?php
session_start();
$_SESSION = [];
session_destroy();

$host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
$uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
$extra = "index.html";
header("Location: http://$host$uri/$extra");                    
?>