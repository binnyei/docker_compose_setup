<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{
    require_once "db access/db_connection.php"; // Einbinden von Datenbankverbindungsaufbau
    
?>                                                                  <!--Von hier bis nach dem else brauch ich das Script zum Session erstellen-->


<html>
<head>    
    <title>Christian's Homepage</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch.  -->
    <link rel="stylesheet" type="text/css" href="style.css"> 
    <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="mobile.css"> 
    <script src="jquery-3.5.1.js"></script>
	<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
    <script src="jquery-ui/jquery-ui.js"></script>
</head>


    
<body>

	<div class="menu_top">
    <ul>

		<li><a href="input_rezepte.php" target="_self">Neu</a></li>
        <li><a href="search_rezepte.php" target="_self">Suchen</a></li> 
        <li><a href="sort_rezepte.php" target="_self">Sortieren</a></li> 
        <li><a href="edit_rezepte.php" target="_self">Editieren</a></li> 
        <div id="topmenu_right">
            <li><a href="logout.php" target="_self">logout</a></li>
        </div>
    </ul>
    </div>
    
    <div class="abstand">
    </div>
	<div class="content">

        
        
        

<!-- Hier wird im selben Script bearbeitet:-->
<!-- <form action="Formulare.php" method="get">  Selben Namen reinschreiben wie das Script in dem wir gerade sind -->
<!-- Den Vornamen lasse ich hier gespeichert, falls keiner gesetzt ist wird der String überschrieben.-->


<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES);?>" method="post">  <!-- Namen von dem jetzigen Script selbst beziehen, damit man es nicht händisch ändern muss wenn man die Struktur ändert-->
	<input type="text" name="rezept_name" size="50" maxlength="100" id="rezept_name" placeholder="Rezeptname" class="input_fields"/>
	<br><br>
	<textarea type="text" name="rezept_zutaten" rows="20" cols="30" id="rezept_zutaten" placeholder="Zutaten" class="input_fields"></textarea>
	<textarea type="text" name="rezept_anleitung" rows="20" cols="50" id="rezept_anleitung" placeholder="Anleitung" class="input_fields"></textarea>
	<br>
    <select name="hauptkategorie" id="hauptkategorie">
        <option value="" id="first_placeholder">Hauptkategorie</option>
    <?php
        //Da kommt der ganze Scheiß mit einer Dropbox befüllen.
        //Ich rufe zuerst meine Tabelle mit den Einträgen auf und fülle dann alles in ein Array ein über eine Schleife.
        //Das alles mache ich innerhalb eines Selects mit mehrfachen Php scripten
        $ergebnis = $mysqli->query("SELECT hauptkategorie FROM hauptkategorie");
        $i=0;
        while($zeile = $ergebnis->fetch_assoc())    // Hier hole ich alle Sätze von einer Tabelle und schreibe sie in ein array
        {
        $menu[$i] = $zeile['hauptkategorie'];
    ?>    
    
        <option value="<?php echo $menu[$i] ?>"><?php echo $menu[$i] ?></option>
    <?php 
        $i++;    
        }
    ?>
  
    </select>

    <select name="unterkategorie" id="unterkategorie">
        <option value="" id="first_placeholder">Unterkategorie</option>
    <?php
        $ergebnis = $mysqli->query("SELECT unterkategorie FROM nebenkategorie"); // Nebenkategorie -> Tabelle, unterkategorie -> Columnname
        $i=0;
        while($zeile = $ergebnis->fetch_assoc())    // Hier hole ich alle Sätze von einer Tabelle und schreibe sie in ein array
        {
        $menu[$i] = $zeile['unterkategorie'];   // Unterkategorie ist der Zeilenname
    ?>    
    
        <option value="<?php echo $menu[$i] ?>"><?php echo $menu[$i] ?></option>
    <?php 
        $i++;    
        }
    ?>
  
    </select>
  <select name="typ" id="typ">
        <option value="" id="first_placeholder">Ernährungstyp</option>
    <?php
        $ergebnis = $mysqli->query("SELECT type FROM typ"); 
        $i=0;
        while($zeile = $ergebnis->fetch_assoc())    
        {
        $menu[$i] = $zeile['type'];   
    ?>    
    
        <option value="<?php echo $menu[$i] ?>"><?php echo $menu[$i] ?></option>
    <?php 
        $i++;    
        }
    ?>
  
    </select>

	<input type="submit" value="Speichern" id="speichern" class="button" disabled="true"/>
</form>
        
   <script>
       // jedes Mal wenn die Maus beim Dropdown "Typ" was ändert wird geschaut ob alles ausgefüllt worden ist: 
       $("#typ").mouseleave(function(){
           var entered_name = $('#rezept_name').val();
           var entered_zutaten = $('#rezept_zutaten').val();
           var entered_anleitung = $('#rezept_anleitung').val();    
           var selected_option_hauptkategorie = $('#hauptkategorie').find(":selected").text();
           var selected_option_unterkategorie = $('#unterkategorie').find(":selected").text();
           var selected_option_typ = $('#typ').find(":selected").text();
           
           if(entered_name!=""&&entered_zutaten!=""&&entered_anleitung!=""){
               if(selected_option_hauptkategorie!="" && selected_option_unterkategorie!="" && selected_option_typ!=""){
                    $("#speichern").attr("disabled", false);
                    }
           }             
       });
    </script>

        <!-- <p>Info: Es muss alles befüllt sein, und bei den Drop-down Menüs überall etwas ausgewählt sein.</p> -->
       

<?php    
    
    if(!empty($_POST["rezept_name"])&&!empty($_POST["rezept_anleitung"])&&!empty($_POST["rezept_zutaten"]))
    {
    if ($stmt = $mysqli->prepare("INSERT INTO rezepte (name, anleitung, zutaten, hauptkategorie, nebenkategorie, type) VALUES (?, ?, ?, ?, ?, ?)")) // in prepare muss der Name der Spalte in der Tabelle stehen
    {   
        $name = $_POST["rezept_name"]; // Im POST muss der Name des Inputs sein
        $anleitung = $_POST["rezept_anleitung"];
        $zutaten = $_POST["rezept_zutaten"];
        $hauptkategorie = $_POST["hauptkategorie"];
        $nebenkategorie = $_POST["unterkategorie"];
        $typ = $_POST["typ"];
        
        $stmt->bind_param("ssssss", $name, $anleitung, $zutaten, $hauptkategorie, $nebenkategorie, $typ);
        $stmt->execute();
        //printf("Error: %s.\n", $stmt->error); //Display error messages if execute does not work!!!
        $stmt->close();
        $mysqli->close();
    }
    else{echo "Hat nicht funktioniert";}
    }    
?>
        
        
        
 </div>
</body>
</html>


<?php
} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");                   
        }