<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{                                                                   
?>                                                                  <!--Von hier bis nach dem else brauch ich das Script zum Session erstellen-->


<!DOCTYPE html>

<html>
    <head>
    <meta charset="UTF-8" />
        <title>geschützter Bereich</title>
        <link rel="stylesheet" type="text/css" href="style.css"> <!-- Link zum CSS Stylesheet -->
</head>

<body>
    <?php   echo "<h1>Hallo {$_SESSION['name']}</h1>";    
    ?>
    <p>Das ist die geschützte Seite</p>
    
    
</body>
</html>    

    
<?php
} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");
        }