<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Variablen in Php</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch -->    
</head>


    
<body>


<?php 
$string1 = "Das ist ein String.";
$integer2 = 4;
echo "$string1 <br /> <br>"; //Kommentare in php

$alter = 12;
$name = "Julia";

echo "$name ist $alter Jahre alt. <br>";
echo "{$name}'s Alter ist $alter. <br> <br>";
echo "Hier kann ich das <strong>Sonderzeichen</strong> \" ausgeben. Das brauche ich für HTML Code. <br> <br>";

$koffer = "Jacke, "; 			//mit .= kann man mehrere Strings addieren.
$koffer .= "T-Shirt und ";
$koffer .= "Vibrator";
echo "Hiermit kann ich an Strings einfach etwas hinzugeben. Ich packe meinen Koffer: $koffer. <br> <br>";
echo "Mit dem nächsten Befehl kann ich eine Variable mit Informationen ausgeben lassen: <br>";
var_dump($koffer);
echo "<br> <br>";

#Arrays:
$array1= [1, 2, "drei", "vier"];
echo "Ausgabe von 0. Stelle des Arrays: $array1[0]. <br>";
echo "Ausgabe von 3. Stelle des Arrays: $array1[3]. <br><br>";
//etwas zum Array hinzufügen:
$array1[] = "siebenundzwanzig";

echo "<strong> Verschiedene Methoden um Arrays auszugeben: </strong> <br>";
print_r($array1);
echo "<br>";
var_dump($array1);

echo "<br> <br>";

//Ein Array ausgeben mittels foreach Schleife:
foreach($array1 as $temp) {echo "$temp <br>";}
echo "<br> <br>";

//Ein Array ausgeben mittels foreach Schleife als ungeordnete Liste:
$bundeslaender = ["Wien", "Burgenland", "Niederösterreich", "Kärnten", "Vorarlberg", "Steiermark", "Oberösterreich", "Tirol", "Salzburg"];
echo "<ul>";
asort($bundeslaender);	//Sortiert das Array. Kann ich nicht direkt bei der Ausgabe machen (als ordered list).
foreach($bundeslaender as $temp) {echo "<li>$temp</li>";}
echo "</ul>";











?>
</body>
</html>