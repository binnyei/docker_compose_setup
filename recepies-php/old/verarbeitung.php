<?php 

// Mittels $_GET['variable'] kann ich per Methode auf das Feld zugreifen:

// echo "Ihre Eingaben:<br>";
// echo "Vorname: {$_GET['vorname']} <br>";
// echo "Nachname: {$_GET['nachname']}";

//Bei der Ausgabe muss ich HTML Spezialzeichen unterbinden (kommt noch später im Buch):

echo "Ihre Eingaben bearbeitet:<br>";
echo "Vorname: ". htmlspecialchars($_GET["vorname"]) . "<br>";	//wenn ich hier zB <br> eingebe kommt kein Zeilenumbruch
echo "Nachname: {$_GET['nachname']} <br>";							//Hier kommt bei Eingabe von <br> ein Zeilenumbruch
echo "Telefon: ". htmlspecialchars($_GET["tel"]) . "<br>";

?>
