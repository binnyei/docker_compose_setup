<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Bilder in Array</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch -->    
</head>

    
<body>

<!-- Aufruf in HTML von Bildern die in einem anderen Ordner liegen: -->
<!-- <img src ="/Zufallsbilder/1.jpg" width="150" height="400"> -->

<?php

//Aufruf von Bilder in anderem Ordner in PHP:
//echo "<img src ='/Zufallsbilder/1.jpg' width='150' height='400'>";

$bilderpfade = [
"<img src ='/Zufallsbilder/1.jpg' width='150' height='400'>",
"<img src ='/Zufallsbilder/2.jpg' width='150' height='400'>",
"<img src ='/Zufallsbilder/3.jpg' width='150' height='400'>",
"<img src ='/Zufallsbilder/4.jpg' width='150' height='400'>",
"<img src ='/Zufallsbilder/5.jpg' width='150' height='400'>",
"<img src ='/Zufallsbilder/6.jpg' width='150' height='400'>"
];

$zufallszahl = rand(0, 5);
echo "$bilderpfade[$zufallszahl]<br><br>";


?>
</body>
</html>