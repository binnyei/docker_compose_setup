<?php
require_once "db access/db_connection.php"; //Einbinden von Datenbankverbindungsaufbau

$ergebnis = $mysqli->query("SELECT * from rezepte WHERE name LIKE '%Bunte%';");          

echo "<table border='1'>";

while($zeile = $ergebnis->fetch_array())
    {
    echo "<tr><td>" . htmlspecialchars($zeile["name"]) . "</td>"
        . "<td>" . nl2br($zeile["zutaten"]) . "</td>" 
        . "<td>" . nl2br($zeile["anleitung"]) . "</td>"
        . "<td>" . htmlspecialchars($zeile["type"]) . "</td>"
        . "<td>" . htmlspecialchars($zeile["hauptkategorie"]) . "</td>"
        . "<td>" . htmlspecialchars($zeile["nebenkategorie"]) . "</td>"
        . "</tr>";
    
    }

echo "</table>";



$mysqli->close(); // Verbindung wird geschlossen



?>