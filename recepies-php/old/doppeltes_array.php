<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Bilder in Array</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch -->    
</head>

    
<body>

<!-- Aufruf in HTML von Bildern die in einem anderen Ordner liegen: -->
<!-- <img src ="/Zufallsbilder/1.jpg" width="150" height="400"> -->

<?php

//Aufruf von Bilder in anderem Ordner in PHP:
//echo "<img src ='/Zufallsbilder/1.jpg' width='150' height='400'>";

$farben = [
["rot" => "#FF0000", "geschmack" => "schön", "magich" => "ja"],
["grün" => "#00FF00", "geschmack" => "naja", "magich" => "nein"],
["blau" => "#0000FF", "geschmack" => "jaschon", "magich" => "vielleicht"],
];

echo "Bei verschachtelten Listen ist es nicht so leicht die richtige Auswahl ausgeben zu lassen. <br> Man gibt am besten den String als Addition dazu.<br>";

echo "Die Farbe ist " .$farben[2]["geschmack"]; //Index 2 -> sucht in der dritten Zeile nach dem Eintrag in Geschmack und gibt diesen aus


?>
</body>
</html>