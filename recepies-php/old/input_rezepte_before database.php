<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{
    require_once "db access/db_connection.php"; // Einbinden von Datenbankverbindungsaufbau
?>                                                                  <!--Von hier bis nach dem else brauch ich das Script zum Session erstellen-->


<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Formulare</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch -->    
</head>


    
<body>




<!-- Hier wird im selben Script bearbeitet:-->
<!-- <form action="Formulare.php" method="get">  Selben Namen reinschreiben wie das Script in dem wir gerade sind -->
<!-- Den Vornamen lasse ich hier gespeichert, falls keiner gesetzt ist wird der String überschrieben.-->


<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES);?>" method="post">  <!-- Namen von dem jetzigen Script selbst beziehen, damit man es nicht händisch ändern muss wenn man die Struktur ändert-->
	<label for="rezept_name">Rezeptname</label>
	<input type="text" name="rezept_name" size="50" maxlength="100" id="rezept_name" value="" />
	<br>
	<label for="rezept_zutaten">Zutaten</label><br>
	<textarea name="rezept_zutaten" rows="10" cols="30" id="rezept_zutaten"></textarea>
	<br><br>
	<label for="rezept_anleitung">Anleitung</label><br>
	<textarea name="rezept_anleitung" rows="50" cols="100" id="rezept_anleitung"></textarea>
	<br>

	<input type="radio" name="rezept_art" value="fleischig" id="fleischig">
	<label for="fleischig">fleischig</label> 
	<input type="radio" name="rezept_art" value="vegetarisch" id="vegetarisch">	
	<label for="vegetarisch">vegetarisch</label> 	
	
	
	
	<input type="submit" value="Abschicken in selben Formular" />
</form>


<!--
Todo: Eingabefelder für kategorien erstellen:
Salate
Vorspeisen
Suppen (Cremesuppen, klare Suppen)
Couscous
Polenta
Reis (diverses, Pfannen, Risotto)
Nudeln (Aufläufe, Pasta, Spätzle, Taschen)
Erdäpfel (Aufläufe, Diverses)
Gemüse (Aufläufe, Diverses, Eintöpfe, Pfannen)
Fleisch (Eintöpfe, Laibchen und Bällchen, Saftfleisch)
Geflügel (Ofen, Pfannen, Spieße)
Strudel
Gebackenes (Brote, Küchlein, Quiches und Tartes, Pizzen)
Beilagen (Knödel)
-->



<!-- Ausgabe in Php: -->
<?php 

if(!empty($_POST["rezept_name"])){echo "Rezeptname: ". htmlspecialchars($_POST["rezept_name"], ENT_QUOTES) . "<br>";}
if(!empty($_POST["rezept_zutaten"])){echo "Zutaten: "."<br>". nl2br(htmlspecialchars($_POST["rezept_zutaten"]), ENT_QUOTES) . "<br>";}
if(!empty($_POST["rezept_anleitung"])){echo "Anleitung: "."<br>". nl2br(htmlspecialchars($_POST["rezept_anleitung"]), ENT_QUOTES) . "<br>";}
if(!empty($_POST["rezept_art"])){echo htmlspecialchars($_POST["rezept_art"], ENT_QUOTES) . "<br>";}

?>

</body>
</html>


<?php
} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");                   
        }