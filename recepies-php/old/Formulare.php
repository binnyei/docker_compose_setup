<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Formulare</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch -->    
</head>


    
<body>

<!-- Einfaches Formular in HTML. Steht im HTML Teil und nicht im php Teil. Maxlength wird nur schlecht überprüft, gehört in php geprüft.-->
<!-- action gibt an in welchem Php Script es bearbeitet wird, method die Methode zum Übermitteln.-->
<!-- Über den Namen des Input Feldes wird per Php auf dieses Feld zugegriffen.-->
<!-- Mit dem label Tag kann man Eingabefelder für Usability markieren. Weiß nicht so genau wofür das ist. Wird mit id verbunden. Hat keine Auswirkung auf Php.-->
<!-- Eine Auwirkung: Wenn ich auf den Namen Vorname klicke springt der Focus direkt in die Eingabemaske!-->

<form action="verarbeitung.php" method="get"> 
	<label for="vorname">Vorname:</label><br>
	<input type="text" name="vorname" size="20" maxlength="30" id="vorname"/>
	<br>
	<label for="nachname">Nachname:</label><br>
	<input type="text" name="nachname" size="20" maxlength="30" id="nachname"/>
	<br>
	<label for="tel">Telefon:</label><br>
	<input type="tel" name="tel" size="20" maxlength="30" id="tel"/>
	<br>
	<input type="submit" value="Abschicken" />
</form>




<!-- Hier wird im selben Script bearbeitet:-->
<!-- <form action="Formulare.php" method="get">  Selben Namen reinschreiben wie das Script in dem wir gerade sind -->
<!-- Den Vornamen lasse ich hier gespeichert, falls keiner gesetzt ist wird der String überschrieben.-->
<?php 
if (isset($_GET["vornameS"])){
	$vornameS = htmlspecialchars($_GET["vornameS"]);
	
}

else{$vornameS="";}

?>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="get">  <!-- Namen von dem jetzigen Script selbst beziehen, damit man es nicht händisch ändern muss wenn man die Struktur ändert-->
	<label for="vornameS">Vorname selbes Formular:</label><br>
	<input type="text" name="vornameS" size="20" maxlength="30" id="vornameS" value="<?php echo $vornameS; ?>" />
	<br>
	<label for="nachnameS">Nachname selbes Formular:</label><br>
	<input type="text" name="nachnameS" size="20" maxlength="30" id="nachnameS"/>
	<br>
	<label for="telS">Telefon:</label><br>
	<input type="telS" name="telS" size="20" maxlength="30" id="telS"/>
	<br>
	<textarea name="kommentar" rows="3" cols="20"></textarea>
	<br>
	
	<input type="submit" value="Abschicken in selben Formular" />
</form>

<?php 
// Hier noch eine Abfrage ob eh etwas im Formular steht:
if(isset($_GET["vornameS"])){
echo "Ihre Eingaben bearbeitet:<br>";
echo "Vorname: ". htmlspecialchars($_GET["vornameS"]) . "<br>";	//wenn ich hier zB <br> eingebe kommt kein Zeilenumbruch
echo "Nachname: {$_GET['nachnameS']} <br>";							//Hier kommt bei Eingabe von <br> ein Zeilenumbruch
echo "Telefon: ". htmlspecialchars($_GET["telS"]) . "<br>";
echo "Kommentar: <br>". nl2br(htmlspecialchars($_GET["kommentar"])) . "<br>"; //nl2br macht Zeilenumbrüche sichtbar bei der Ausgabe. Ansonsten steht alles in einer Wurst
}
?>








<?php
//if ($_GET["sichtbar"]==true){

	
if (!isset($_GET["sichtbar"])){	
	echo "Ihre Eingaben:<br>";
	echo "Vorname: ". htmlspecialchars($_GET["vorname3"]) . "<br>";
	echo "Nachname: ". htmlspecialchars($_GET["nachname3"]) . "<br>";
	echo "Telefonnummer: ". htmlspecialchars($_GET["tel3"]) . "<br>";
?>

<!-- Checkbox eingebaut um Formular nach absenden sichtbar zu machen oder nicht:-->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="get">  
	<label for="vorname3">Vorname selbes Formular:</label><br>
	<input type="text" name="vorname3" size="20" maxlength="30" id="vorname3"/>
	<br>
	<label for="nachname3">Nachname selbes Formular:</label><br>
	<input type="text" name="nachname3" size="20" maxlength="30" id="nachname3"/>
	<br>
	<label for="tel3">Telefon:</label><br>
	<input type="tel" name="tel3" size="20" maxlength="30" id="tel3"/>
	<br>
	<label for="sichtbar">Unsichtbarmachen?</label>
	<input type="checkbox" id="sichtbar" name="sichtbar" value="Sichtbar">
	<br>
	<input type="submit" value="Abschicken in selben Formular" />
</form>

<?php 
}
else{echo "wird nicht angezeigt";}

?>

</body>
</html>