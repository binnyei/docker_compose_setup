<?php
session_start(); //Startet die Session. Muss möglichst zu Beginn stehen im Script

$host = $_SERVER["HTTP_HOST"];
$uri = rtrim(dirname($_SERVER["PHP_SELF"]), "/");

//Benutzer die ich manuell angelegt habe:
$benutzer_array = [
                "user" => "123QWEasd",
                "christian" => "123QWEasd"
            ];

//Überprüfung ob beim Passwort oder Benutzername etwas eingegeben worden ist. Wenn nicht wird alles mit Leerzeichen überschrieben. Ansonsten werden die Eingabedaten in _temp Variablen gespeichert.
//Überprüft keine Leerzeichen

if($_POST['benutzername'] == "" || $_POST['passwort'] == ""){
    echo "keine Eingabe gewählt";
    $benutzer_temp = "";
    $passwort_temp = "";
    }
    else{
    $benutzer_temp = $_POST['benutzername'];
    $passwort_temp = $_POST['passwort'];
    }

// Mit der if-Anweisung prüfe ich ob mein _temp Benutzer in meinem Array gespeichert ist. Wenn ja vergleiche ich das Passwort. Wenn nein dann gehe ich mittels $extra und einem code f=1 zurück.
if(!array_key_exists($benutzer_temp, $benutzer_array)){
    $extra = "index.html?f=1";                                      //ruft die index.html mit index.html?1 auf wenn der Benutzer nicht existiert
    }
    elseif($benutzer_array[$benutzer_temp] != $passwort_temp){
        $extra = "index.html?f=2";                                  //ruft index.html mit index.html?f=2 auf wenn der Benutzer existiert aber das Passwort falsch ist
        
        }
    
        else{
            $extra = "search_rezepte.php";
            $_SESSION["login"] = "ok";                              //hier wird in die Session in die Felder login bzw name die Daten gespeichert
            $_SESSION["name"] = $benutzer_temp;
            }

header("Location: https://$host$uri/$extra");                                         //Gehe hier hin mit dem Parameter den ich aus $extra bekommen

?>



