<?php
                if(isset($_POST['rezept_name'])){
                $suche=$_POST['rezept_name']; 
                $suche= "%".$suche."%";
                //Suche nach Namen oder Zutaten. Ich kann die Suche je nach Auswahl von Radiobutton "suche_Auswahl" vornehmen
                if($_POST['suche_auswahl']=='Name'){
                             if ($stmt = $mysqli->prepare("SELECT id, name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE name LIKE ? ORDER BY hauptkategorie, nebenkategorie")) 
                            {
                                $stmt->bind_param("s", $suche);
                                $stmt->execute();

                                //printf("Error: %s.\n", $stmt->error); //Display error messages if execute does not work!!!

                                $stmt->bind_result($id, $name, $anleitung, $zutaten, $type, $hauptkategorie, $nebenkategorie);
                                
                                
                                echo "<table>\n";
                                echo "<tr>\n";
                                echo "<th>Id</th>";    
                                echo "<th>Name</th>";
                                echo "<th>Zutaten</th>";
                                echo "<th>Anleitung</th>";
                                echo "<th>Hauptkategorie</th>";
                                echo "<th>Unterkategorie</th>";
                                echo "<th>Ernährungstyp</th>";
                                echo "</tr>";
                                while($stmt->fetch()){
                                    echo "<tr>\n\t<td>"
                                        .htmlspecialchars($id)
                                        ."</td>\n\t<td>"
                                        .htmlspecialchars($name)
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($zutaten))        // Gibt den Zeilenumbruch an aber macht auch <> unschädlich!
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($anleitung))
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($hauptkategorie))
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($nebenkategorie))
                                        ."</td>\n\t<td>"
                                        .nl2br(htmlspecialchars($type))
                                        ."</td></tr>";
                                }
                                    


                                $stmt->close();
                                $mysqli->close();

                            }

                            else{echo ("Hat nicht funktioniert");}
                          
                    }
                        else{
                               if ($stmt = $mysqli->prepare("SELECT id, name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE zutaten LIKE ? ORDER BY hauptkategorie, nebenkategorie")) 
                                {
                                    $stmt->bind_param("s", $suche);
                                    $stmt->execute();

                                    //printf("Error: %s.\n", $stmt->error); //Display error messages if execute does not work!!!

                                    $stmt->bind_result($id, $name, $anleitung, $zutaten, $type, $hauptkategorie, $nebenkategorie);
                                    
                                    echo "<table>\n";
                                    echo "<tr>\n";
                                    echo "<th>Id</th>";    
                                    echo "<th>Name</th>";
                                    echo "<th>Zutaten</th>";
                                    echo "<th>Anleitung</th>";
                                    echo "<th>Hauptkategorie</th>";
                                    echo "<th>Unterkategorie</th>";
                                    echo "<th>Ernährungstyp</th>";
                                    echo "</tr>";
                                    while($stmt->fetch()){
                                        echo "<tr>\n\t<td>"
                                            .htmlspecialchars($id)
                                            ."</td>\n\t<td>"
                                            .htmlspecialchars($name)
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($zutaten))        // Gibt den Zeilenumbruch an aber macht auch <> unschädlich!
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($anleitung))
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($hauptkategorie))
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($nebenkategorie))
                                            ."</td>\n\t<td>"
                                            .nl2br(htmlspecialchars($type))
                                            ."</td></tr>";
                                    }


                                    $stmt->close();
                                    $mysqli->close();

                                }
                               

                                else{echo ("Hat nicht funktioniert");}
                               
                    
                }

                                  
                    
                    

                    
                    
                    }
            ?> 