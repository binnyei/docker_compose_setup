<?php
session_start();                                                    //startet eine Session
if(isset($_SESSION["login"]) && $_SESSION["login"] == "ok")         //Ist eine Session gesetzt und ist die Session OK
{
    require_once "db access/db_connection.php"; // Einbinden von Datenbankverbindungsaufbau
    
?>                                                                  <!--Von hier bis nach dem else brauch ich das Script zum Session erstellen-->


<html>
<!-- Kommentare in HTML -->
    
<head>    
    <title>Christian's Homepage</title> <!-- Titel der Homepage im Tab, etc. -->
    <meta http-equiv="content-type" content="text/html"; charset="utf-8" /> <!-- Sonderzeichen deutsch.  -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="mobile.css"> 
    <script src="jquery-3.5.1.js"></script>
	<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
    <script src="jquery-ui/jquery-ui.js"></script>
</head>


    
<body>
	<div class="menu_top">
    <ul>
		<li><a href="input_rezepte.php" target="_self">Neu</a></li>
        <li><a href="search_rezepte.php" target="_self">Suchen</a></li> 
        <li><a href="sort_rezepte.php" target="_self">Sortieren</a></li> 
        <li><a href="edit_rezepte.php" target="_self">Editieren</a></li> 
        <div id="topmenu_right">
            <li><a href="logout.php" target="_self">logout</a></li>
        </div>
    </ul>
    </div>
    
    <div class="abstand">
        
    </div>
	<div class="content">


<!-- Hier wird im selben Script bearbeitet:-->
<!-- <form action="Formulare.php" method="get">  Selben Namen reinschreiben wie das Script in dem wir gerade sind -->
<!-- Den Vornamen lasse ich hier gespeichert, falls keiner gesetzt ist wird der String überschrieben.-->


<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"], ENT_QUOTES);?>" method="post">  <!-- Namen von dem jetzigen Script selbst beziehen, damit man es nicht händisch ändern muss wenn man die Struktur ändert-->
	<input type="text" name="rezept_id" class="input_fields" size="5" maxlength="100" id="rezept_id" value="" placeholder="Rezept ID"/>
	<input type="submit" value="Suchen" id="suchen" class="button"/>
</form>
        
<?php    // SUCHE NACH ID UND EINTRAGEN IN TEXTBOXEN
    
    if(isset($_POST['rezept_id'])){
    $suche=$_POST['rezept_id']; 
        
    if ($stmt = $mysqli->prepare("SELECT id, name, anleitung, zutaten, type, hauptkategorie, nebenkategorie from rezepte WHERE id=?")) 
    {
        $stmt->bind_param("s", $suche);
        $stmt->execute();
        $stmt->bind_result($id, $name, $anleitung, $zutaten, $type, $hauptkategorie, $nebenkategorie);
        $result = $stmt->get_result();
        while($zeile = $result->fetch_array()){
        
    ?>    
        
        <!-- Update is made in update_rezepte.php -->
<form action="update_rezepte.php" method="post">
    <?php echo "ID: ".nl2br(htmlspecialchars($zeile['id'])) ?>
<!--    <?php echo htmlspecialchars($hauptkategorie) ?><?php echo htmlspecialchars($zeile["hauptkategorie"]) ?>
    <?php echo htmlspecialchars($nebenkategorie) ?>/<?php echo htmlspecialchars($zeile["nebenkategorie"]) ?>
    <?php echo htmlspecialchars($type) ?>/<?php echo htmlspecialchars($zeile["type"]) ?>
    <br>
-->    <br>
    
    <input type="hidden" name="rezept_id_post"  id="rezept_id_post" size="10" maxlength="10" id="rezept_id_post" value="<?php echo nl2br(htmlspecialchars($zeile["id"])) ?>" />
    <br>
	<input type="text" name="rezept_name" id="rezept_name" class="input_fields" size="50" maxlength="100" id="rezept_name" value="<?php echo nl2br(htmlspecialchars($zeile["name"])) ?>" />
    <br><br>
	
	<textarea name="rezept_zutaten" id="rezept_zutaten" class="input_fields" rows="20" cols="30" id="rezept_zutaten"><?php echo htmlspecialchars($zeile["zutaten"]); ?></textarea>
	<textarea name="rezept_anleitung" id="rezept_anleitung" class="input_fields" rows="20" cols="50" id="rezept_anleitung"><?php echo htmlspecialchars($zeile["anleitung"]); ?></textarea>
	<br>
    <br>
    <button id="edit" type="submit" class="button">Speichern</button>

</form>

   <?php     
    }
        $stmt->close();
        $mysqli->close();
    }
        }
?>   
        
        
    </div>

    
    


    
    
    
    
    
    
    
    
</body>
</html>


<?php
} else  {                                                         //Wenn die Session nicht OK ist soll er zurück zur Index Seite gehen
    $host = htmlspecialchars($_SERVER["HTTP_HOST"]);              //Oder ein else erstellen mit einer Fehlermeldung
    $uri = rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
    $extra = "index.html";
    header("Location: http://$host$uri/$extra");                   
        }