from flask import Flask, render_template, session, request, redirect, url_for
import sys
from sqlalchemy import inspect, select, and_, not_, update, desc
import sqlalchemy
import random
from datetime import datetime




app = Flask(__name__)
app.secret_key="supersecret"


#https://pythonise.com/series/learning-flask/flask-session-object
#https://codeshack.io/login-system-python-flask-mysql/
#SQLALCHEMY tutorial: https://towardsdatascience.com/sqlalchemy-python-tutorial-79a577141a91

########################
#Connection to MariaDB:
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://db_user:123QWEasd@192.168.0.2:3306/database_cv' #Connection to MariaDB and Bus Relation
engine = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI, echo=True) # Engine represents the core interface of the DB
connection = engine.connect()
metadata=sqlalchemy.MetaData()

#Load data from table:
education=sqlalchemy.Table("education", metadata, autoload=True, autoload_with=engine)
experience=sqlalchemy.Table("experience", metadata, autoload=True, autoload_with=engine)



@app.route("/")
def home():
#  if session:
#    get_person = sqlalchemy.select([Person]).where(Person.columns.SVNGeb==session['id'])
#    result_temp = connection.execute(get_person)  # Query is executed and temporarly saved
#    account = result_temp.fetchone()  # result is bind to list
#    return render_template('home.html', msg="Logged in as "+ account.Vorname + " " + account.Nachname)

  stmt = sqlalchemy.select([education]).order_by(sqlalchemy.desc(education.columns.Id))
  edu_results = connection.execute(stmt).fetchall()

  return render_template('home.html', elements=edu_results)
  #renders from the template folder home.html. Folder has to be named templates

@app.route("/workexperience")
def workexperience():
  stmt = sqlalchemy.select([experience]).order_by(sqlalchemy.desc(experience.columns.Id))
  work_results = connection.execute(stmt).fetchall()

  return render_template('workexperience.html', elements=work_results)


if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=True, port=5000) #127.0.0.1:500 => after running python script it is available over this URL_Port